package com.example.mohammedmobinmunir.myapplication

/**
 * Created by MohammedMobinMunir on 2/21/2018.
 */
interface OnValueListener {
    fun onValue(value: Int)
}