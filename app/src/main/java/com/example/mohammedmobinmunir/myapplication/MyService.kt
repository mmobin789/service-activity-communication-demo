package com.example.mohammedmobinmunir.myapplication

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import java.util.concurrent.TimeUnit

class MyService : Service() {

    companion object {
        private var value = 0
        private var onValueListener: OnValueListener? = null
        fun setonValueListener(onValueListener: OnValueListener) {
            this.onValueListener = onValueListener
        }
    }


    private fun updateValue() {

        Handler().postDelayed({
            onValueListener?.onValue(value)
            if (value == 100) {
                value = 0
            } else
                value++
            updateValue()


        }, TimeUnit.SECONDS.toMillis(1))
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i("onStartCommand", "Works")
        updateValue()
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        return null
    }
}
