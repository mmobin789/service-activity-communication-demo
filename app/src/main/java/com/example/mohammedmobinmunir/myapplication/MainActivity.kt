package com.example.mohammedmobinmunir.myapplication

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnValueListener {

    override fun onValue(value: Int) {
        tv.text = value.toString()
    }


    private fun isMyServiceRunning(serviceClass: Class<Service>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        return manager.getRunningServices(1000).any { serviceClass.name == it.service.className }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myService = MyService()
        MyService.setonValueListener(this)
        if (!isMyServiceRunning(myService.javaClass))
            startService(Intent(this@MainActivity, myService.javaClass))

    }


}
